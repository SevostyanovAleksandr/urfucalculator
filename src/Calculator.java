import java.util.Scanner;
public class Calculator {
    public static void main(String[] args) {
        Double result;
        String event;
        Scanner reader = new Scanner(System.in);
        System.out.print("Введите первое число:\n");
        Double number1 = reader.nextDouble();
        System.out.print("Введите второе число:\n");
        Double number2 = reader.nextDouble();
        System.out.print("Введите оператор вычисления: (+, -, *, /, sin, cos): ");
        event = reader.next();

        switch(event) {
            case "+":
                 result = number1 + number2;  //сложени
                break;
            case "-" : result = number1 - number2; // вычитание
                break;
            case "*" : result = number1 * number2;  // умножени
                break;
            case "/" : result = number1 / number2; // деление
                break;
            case "sin" : result = Math.sin(number1);
			case "cos" : result = Math.cos(number1);
            break;
            case "tg" : result = Math.tan(number1);
            break;
            default:  System.out.printf("Ошибочка введите оператор вычисления из предложенных");
                return;
        }
        System.out.println("Итого у нас получилось:" + number1 + " " + event + " " + number2 + " = " + result);
    }
}